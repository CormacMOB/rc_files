colorscheme jellybeans

iab pdb debugger;

syntax region foldBraces start=/{/ end=/}/ transparent fold keepend extend
setlocal foldmethod=syntax
setlocal foldlevel=99

let javascript_fold=1
