execute pathogen#infect()                                                                                                                                                                       
                                                                                                                                                                                                
"auto indentation                                                                                                                                                                               
set cindent                                                                                                                                                                                     
                                                                                                                                                                                                
set backspace=2                                                                                                                                                                                 
                                                                                                                                                                                                
"Higligths keywords                                                                                                                                                                             
syntax on                                                                                                                                                                                       
                                                                                                                                                                                                
                                                                                                                                                                                                
set autoindent                                                                                                                                                                                  
                                                                                                                                                                                                
"Tabs                                                                                                                                                                                           
set expandtab "remplaces tabs with spaces                                                                                                                                                       
set shiftwidth=4                                                                                                                                                                                
set softtabstop=4                                                                                                                                                                               
set tabstop=4                                                                                                                                                                                   
                                                                                                                                                                                                
"Line Numbers                                                                                                                                                                                   
                                                                                                                                                                                                
set number                                                                                                                                                                                      

" Abbrebviations and spelling filters
iab teh the
iab Teh The
iab comit commit
iab commmit commit
iab charachter character

" look and feel

set t_Co=256
set background=dark
colorscheme peachpuff
set modeline
set ls=2

filetype on
filetype plugin on
set omnifunc=syntaxcomplete#Complete
let g:jelleybeans_overrides = {
\     'Todo': { 'guifg': '303030', 'guibg': 'f0f000',
\              'ctermfg': 'Black', 'ctermbg': 'Yellow',
\              'attr': 'bold' },
\    'Comment': { 'guifg': 'cccccc' },
\}

"Insight configs to json
autocmd BufRead,BufNewFile ~/Openapp/*/config*/{htmlform,template,dbview,edceventtype,edcstudy,dd,report,terminology,dbview,custom_tables}/* set filetype=json

